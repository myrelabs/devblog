#include "hal.h"
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
void my_puts(char *c) {
  while(*c!=0)
    putch(*c++);
}
void my_read(char *b, int len) {
  while(len--) {
    *b++ = getch();
  }
}
int main(void) {
  platform_init();
  init_uart();
  trigger_setup();
  char q[8];
  char p[sizeof(q)];
  while(1) {
    for(unsigned i = 0;i<sizeof(q);i++) {
      p[i] = 0;
      q[i] = 1;
    }
    my_read(q, sizeof(q));
    my_read(p, sizeof(p));
    trigger_high();
    unsigned c = 0;
    for(unsigned i=0; i<sizeof(q);i++) {
      c  |= q[i] ^ p[i];
    }
    trigger_low();
    if(c == 0) {
      my_puts("Pass");
    }
    else{
      my_puts("Fail");
    }
  }
  return 0;
}
